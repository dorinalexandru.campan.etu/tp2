const data = [
	{
		name: 'Regina',
		base: 'tomate',
		price_small: 6.5,
		price_large: 9.95,
		image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
	},
	{
		name: 'Napolitaine',
		base: 'tomate',
		price_small: 6.5,
		price_large: 8.95,
		image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
	},
	{
		name: 'Spicy',
		base: 'crème',
		price_small: 5.5,
		price_large: 8,
		image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300',
	}
];
class Component{
	attribute;
	name;
	children

	constructor( name, attribute, children ) {
		this.name = name;
		this.children=children;
		this.attribute=attribute;
		
	}
	render(){
		if(this.children == null || this.children == undefined){
			return  `<${this.name} ${this.attribute.name}="${this.attribute.value}"/>` 
		}else{
			return `<${this.name}>${this.children}</${this.name}>`;
		}
	}
}
class Img extends Component{
	constructor(attribute){
		super('img',{name:"src", value:attribute});
	}
}

const title = new Component( 'h1',null, 'La carte' );
const img = new Img('https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300');
document.querySelector('.pageTitle').innerHTML = title.render();
document.querySelector( '.pageContent' ).innerHTML = img.render();
